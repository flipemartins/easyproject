//
//  Loger.swift
//  EasyProject
//
//  Created by Aulas on 27/05/18.
//  Copyright © 2018 Desenvolvimento iOS. All rights reserved.
//

import Foundation

final class Loger {
    
    // MARK: Nested Types
    
    private enum LogerLPermissionLevel {
        case error
        case warning
        case info
    }
    
    // MARK: Type Alias
    
    private typealias LogerPermissions = [LogerLPermissionLevel]
    
    // MARK: Instance Properties
    
    // MARK: Static Propertie
    
    private static let logerPermissionLevel : LogerPermissions = [.error, .warning]
    
    // MARK: Initializers
    
    private init(){}
    
    // MARK: Instance Methods
    
    // MARK: Static Methods
    
    open static func error (message : String) {
        guard Loger.logerPermissionLevel.contains(.error) else {return}
        print("[LOGGER ERROR]: \(message)")
    }
    
    open static func warning (message : String) {
        guard Loger.logerPermissionLevel.contains(.warning) else {return}
        print("[LOGGER WARNING]: \(message)")
    }
    
    open static func info (message : String) {
        guard Loger.logerPermissionLevel.contains(.warning) else {return}
        print("[LOGGER INFO]: \(message)")
    }
    
}
