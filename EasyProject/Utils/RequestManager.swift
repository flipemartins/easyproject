//
//  RequestManager.swift
//  EasyProject
//
//  Created by Aulas on 27/05/18.
//  Copyright © 2018 Desenvolvimento iOS. All rights reserved.
//

import Foundation
import SystemConfiguration

public class Reachability {
    
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
        
    }
}

public class RequestManager {
    
    // MARK: Nested Types
    
    public enum HTTMPMethod : String {
        case get = "GET"
        case post = "POST"
        case put = "PUT"
        case path = "PATCH"
        case delete = "DELETE"
    }
    
    public enum Result {
        case error (Fail)
        case sucess (Success)
    }
    
    public enum RequestManagerError : Error{
        case invalidURL
        case invalidArguments
        case nilData
        case noNetworkConnection
        case badStatusCode
    }
    
    public struct Success {
        
        let data : Data
        let statusCode : Int?
    }
    
    public struct Fail {
        let error : Error
        let statusCode : Int?
    }
    
    public enum ContentType : String {
        case json = "application/json; charset=utf-8"
    }
    
    // MARK: Type Alias
    public typealias RequestManagerCompletionHandler = (Result) -> Void
    
    // MARK: Properties
    
    // MARK: Initializers
    private init (){}
    
    // MARK: Instance Methods
    
    // MARK: Static Methods
    
    public static func request(stringURL : String, httpMethod : HTTMPMethod = .get, arguments : [AnyHashable:AnyObject]? = nil, contentType : ContentType = .json, extraHeaderFields : [String:String]? = nil, completion : @escaping RequestManagerCompletionHandler) {
        
        guard Reachability.isConnectedToNetwork() else {
            
            Loger.warning(message: "No Network Connection")
            completion(.error(Fail(error: RequestManagerError.noNetworkConnection, statusCode: nil)))
            return
        }
        
        guard let requestURL = URL(string: stringURL) else {
            completion(.error(Fail(error: RequestManagerError.invalidURL, statusCode: nil)))
            Loger.error(message: "Couldn't create URL")
            return
        }
        
        // Request
        var request = URLRequest(url: requestURL)
        request.httpMethod = httpMethod.rawValue
        
        // Body
        if let arguments = arguments {
            
            guard let httpBodyData = try? JSONSerialization.data(withJSONObject: arguments, options: []) else {
                completion(.error(Fail(error: RequestManagerError.invalidArguments, statusCode: nil)))
                Loger.error(message: "Couldn't convert arguments in data")
                return
            }
            
            request.httpBody = httpBodyData
        }
        
        
        // Header
        var hearderFields :  [String:String] = [:]
        
        hearderFields["Content-Type"] = contentType.rawValue
        
        if let extraHeaderFields = extraHeaderFields{
            
            for (k,v) in extraHeaderFields{
                hearderFields[k] = v
            }
        }
        
        hearderFields.forEach { (k,v) in
            request.setValue(v, forHTTPHeaderField: k)
        }
        
        // Session
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode < 300 && httpResponse.statusCode >= 200 else{
                
                Loger.warning(message: "Status code out of success range: \((response as? HTTPURLResponse)?.statusCode ?? 0))")
                
                DispatchQueue.main.async {
                completion(.error(Fail(error: RequestManagerError.badStatusCode, statusCode: (response as? HTTPURLResponse)?.statusCode)))
                }
                return
            }
            
            if let error = error {
                DispatchQueue.main.async {
                    completion(.error(Fail(error: error, statusCode: httpResponse.statusCode)))
                }
                Loger.error(message: "Request result in error: \(error)")
                return
            }
            
            guard let data = data else {
                DispatchQueue.main.async {
                    completion(.error(Fail(error: RequestManagerError.nilData, statusCode: httpResponse.statusCode)))
                }
                Loger.error(message: "Request from \(stringURL) returns a nil data")
                return                
            }
            
            DispatchQueue.main.async {
                completion(.sucess(Success(data: data, statusCode: httpResponse.statusCode)))
                
            }
            
        }.resume()
        
    }
    
}
