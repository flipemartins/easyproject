//
//  Extensions.swift
//  EasyProject
//
//  Created by Aulas on 01/06/18.
//  Copyright © 2018 Desenvolvimento iOS. All rights reserved.
//

import Foundation
import  UIKit
import GoogleMaps

extension UIAlertController {
    
    static func showSimpleAlert(controller : UIViewController, title : String, message : String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(okAction)
        
        controller.present(alert, animated: true, completion: nil)
    }
}


extension GMSMapView {
    
    func putMarkersFor(taxis : [Taxi], centralizeAt: CLLocationCoordinate2D? = nil) {
        
        taxis.forEach { (taxi) in
            
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: taxi.latitude, longitude: taxi.longitude)
            marker.title = taxi.driverName
            marker.snippet = taxi.driverCar
            marker.icon = #imageLiteral(resourceName: "taxi")
            marker.map = self
            
            if let centralizeAt = centralizeAt {
                self.animate(with: GMSCameraUpdate.setTarget(centralizeAt, zoom: 15.0))
            }
        }
    }
}
