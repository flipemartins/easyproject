//
//  Taxy.swift
//  EasyProject
//
//  Created by Aulas on 27/05/18.
//  Copyright © 2018 Desenvolvimento iOS. All rights reserved.
//

import Foundation

struct Taxi{


    // MARK: Nested Types
    
    // MARK: Type Alias
    
    // MARK: Properties
    
    var latitude : Double
    var longitude : Double
    var driverName : String
    var driverCar : String
    
    // MARK: Initializers
    
    init? (from representation : [AnyHashable:AnyObject]){
        
        guard let latitude = representation ["lat"] as? Double, let longitude =  representation ["lng"] as? Double, let driverName = representation ["driver-name"] as? String, let driverCar = representation ["driver-car"] as? String else {
            Loger.error(message: "Couldn't initialize a taxy from representation: \(representation)")
            return nil
        }
        
        self.latitude = latitude
        self.longitude = longitude
        self.driverName = driverName
        self.driverCar = driverCar
    }
    
    // MARK: Instance Methods
    
    // MARK: Static Methods

    
}

extension Taxi : CustomStringConvertible{
    
    var description: String {
        return "Latitude: \(self.latitude) \nLongitude: \(self.longitude) \nDriver Name: \(self.driverName) \nDriver Car: \(self.driverCar) \n"
    }
    
    
    
}


struct TaxiManager {
    
    // MARK: Nested Types
    
    enum TaxiManagerError : Error{
    
        case badStatusCode
        case noNetworkConnection
        case noResultsFound
        case coldNotInitializeTaxis
        case coldNotGetRawTaxis
        case unknow
        
        
        var localizedDescription: String {
            
            switch self {
            case .badStatusCode:
                return "Error requesting the API data"
            case .noNetworkConnection:
                return "Connection Problem"
            case .noResultsFound:
                return "No results found"
            default:
                return "Unexpected Error"
            }
            
        }
        
    }
    
    enum Result {
        case error(TaxiManagerError)
        case success([Taxi])
    }
    
 

    
    
    // MARK: Type Alias
    
    // MARK: Properties
    
    // MARK: Initializers
    
    // MARK: Instance Methods
    
    // MARK: Static Methods
    
    static func getTaxis(latitude : Double, longitude: Double, completion : @escaping (Result) -> Void){
        
        let stringURL = "http://192.168.0.7:8888/api/gettaxis?lat=\(latitude)&lng=\(longitude)"
        
            RequestManager.request(stringURL: stringURL) { (result) in
            switch result {
                
            case .error(let fail):
                
                print("Caiu no error")
                switch fail.error{
                    case RequestManager.RequestManagerError.noNetworkConnection:
                        completion(.error(TaxiManager.TaxiManagerError.noNetworkConnection))
                    case RequestManager.RequestManagerError.badStatusCode:
                        completion(.error(TaxiManager.TaxiManagerError.badStatusCode))
                    case RequestManager.RequestManagerError.nilData:
                        completion(.error(TaxiManager.TaxiManagerError.noResultsFound))
                    default:
                        Loger.warning(message: "[Taxi Manager] Without specific treatment, completion call with unknow error")
                        completion(.error(TaxiManager.TaxiManagerError.unknow))
                }
                
            case .sucess(let success):
                
                guard let rawObject = try? JSONSerialization.jsonObject(with: success.data, options: .allowFragments), let returnObject = rawObject as? [AnyHashable:AnyObject], let rawTaxis = returnObject["taxis"] as? [[AnyHashable:AnyObject]] else {
                    
                    Loger.error(message: "[Taxi Manager] Couldn't get raw taxis")
                    completion(.error(TaxiManager.TaxiManagerError.coldNotGetRawTaxis))
                    return
                }
                
                var taxis : [Taxi] = []
                
                rawTaxis.forEach({ (rawTaxi) in
                    
                    if let taxi = Taxi(from: rawTaxi){
                        taxis += [taxi]
                    }else{
                        Loger.warning(message: "Coldn't initialize a taxy")
                    }
                })
                
                completion(.success(taxis))
            }
 
        }
        
    }
    
}
