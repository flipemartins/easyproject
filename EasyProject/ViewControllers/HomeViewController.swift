//
//  HomeViewController.swift
//  EasyProject
//
//  Created by Aulas on 01/06/18.
//  Copyright © 2018 Desenvolvimento iOS. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class HomeViewController: UIViewController {
    
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var resultMark = GMSMarker()
    var shouldFixOnUserLocation = true
    
    // MARK: - IBOutltes
    
    @IBOutlet weak var myMapView: GMSMapView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var topViewContainer: UIView!
    
    // MARK: - Propriedades

    var locationManager = CLLocationManager()
    
    // MARK: - View Life Cycle
    

    override func viewDidLoad() {
        
        self.setupSearchPlaces()
        self.setupLocationManager()
        self.myMapView.isMyLocationEnabled = true
        self.myMapView.delegate = self
        
    }
    
    // MARK: - Métodos
    
    func changeResultMark(position : CLLocationCoordinate2D, labelText: String?){
        
        if let labelText = labelText {
            self.addressLabel.text = labelText
        }
        
        self.resultMark.map = self.myMapView
        self.resultMark.position = position
        
    }
    
    private func setupSearchPlaces(){
        
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        
        self.topViewContainer.addSubview((searchController?.searchBar)!)
        searchController?.searchBar.sizeToFit()
        searchController?.hidesNavigationBarDuringPresentation = false
        
        // When UISearchController presents the results view, present it in
        // this view controller, not one further up the chain.
        definesPresentationContext = true
        
    }
    
    private func setupLocationManager() {
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
    }
    
    
    func putTaxisForLocation(lat : Double, lng : Double) {
        
        self.activityIndicator.startAnimating()
        TaxiManager.getTaxis(latitude: lat, longitude: lng) { (result) in
            self.activityIndicator.stopAnimating()
            switch result{
                
            case .error(let error):
                UIAlertController.showSimpleAlert(controller: self, title: "Oops!", message: error.localizedDescription)
            case .success(let taxis):
                self.myMapView.putMarkersFor(taxis: taxis, centralizeAt: CLLocationCoordinate2D(latitude: lat, longitude: lng))
                
                break
            }
            
        }
        
    }
    
    // MARK: - Actions
    
    @IBAction func recenterButtonPressed(_ sender: UIButton) {
        
        self.shouldFixOnUserLocation = true
        guard let currentCoordinate = self.locationManager.location?.coordinate else {
            Loger.warning(message: "[HOME VIEW COTRLLER] - Cant get user location to recenter map")
            return
        }

        self.myMapView.animate(with: GMSCameraUpdate.setTarget(currentCoordinate, zoom: 15.0))
        
        addressFrom(location: currentCoordinate) { (address) in
            
            if let address = address {
                self.addressLabel.text = address
            }else{
                Loger.warning(message: "[HOME VIEW CONTROLLER: No addres found for address label]")
            }
            
        }
     
    }
    
    
    
    @IBAction func loadTaxisButtonPressed(_ sender: UIButton) {
        
        /* Tratativa para buscar os taxis
         
         Prioridades: Pino se estiver visível
         Localização do usuário
         centro do mapa
        */
        self.myMapView.clear()
        
        var locationToLoadTaxis = CLLocationCoordinate2D()
        
        if self.myMapView.projection.contains(self.resultMark.position) {
        locationToLoadTaxis = self.resultMark.position
        self.changeResultMark(position: locationToLoadTaxis, labelText: nil)
        }else if let currentLocation = self.locationManager.location?.coordinate, self.myMapView.projection.contains(currentLocation) {
        locationToLoadTaxis = currentLocation
        }else{
        locationToLoadTaxis = self.myMapView.projection.coordinate(for: self.myMapView.center)
            self.changeResultMark(position: locationToLoadTaxis, labelText: nil)
        }
        
        self.putTaxisForLocation(lat: locationToLoadTaxis.latitude, lng: locationToLoadTaxis.longitude)
        self.addressFrom(location: locationToLoadTaxis, completion: { (textAddress) in
           self.addressLabel.text = textAddress
        })
        
    }
    
}

extension HomeViewController : CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        print("Passou Fora")
        
        if shouldFixOnUserLocation{
        
            print("Passou dentro")
            guard  let currentLocation = locations.last else {
                Loger.error(message: "[Home View Controller] Can't get current location")
                return
            }
            
            self.myMapView.animate(with: GMSCameraUpdate.setTarget(currentLocation.coordinate, zoom: 15.0))
            
            addressFrom(location: currentLocation.coordinate) { (address) in
                
                if let address = address {
                    self.addressLabel.text = address
                }else{
                    Loger.warning(message: "[HOME VIEW CONTROLLER: No addres found for address label]")
                }
                
            }
        }
        
    }
    
    func addressFrom(location : CLLocationCoordinate2D, completion : @escaping (String?)->Void){
        
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(location) { (response, error) in
            
            if let error = error {
                Loger.error(message: "[HOME VIEW CONTROLLER] Error occur on reverse geocoder: \(error.localizedDescription)")
                completion(nil)
                return
            }
            
            guard let response = response else {
                Loger.error(message: "[HOME VIEW CONTROLLER] Nil Response on reverse geocoder")
                completion(nil)
                return
            }
            
            guard let address = response.firstResult() else {
                Loger.error(message: "[HOME VIEW CONTROLLER] No address found")
                completion(nil)
                return
            }
            
            
            var addressString = ""
            if let lines = address.lines {
                lines.forEach({ (line) in
                    addressString += "\(line)\n"
                })
            }else{
                Loger.warning(message: "[HOME VIEW CONTROLLER] No lines from address")
                completion(nil)
                return
            }
            completion(addressString)
        }
        
    }
}


extension HomeViewController: GMSAutocompleteResultsViewControllerDelegate {
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didAutocompleteWith place: GMSPlace) {
        
        searchController?.isActive = false
        
        self.myMapView.animate(with: GMSCameraUpdate.setTarget(place.coordinate, zoom: 15.0))
        self.changeResultMark(position: place.coordinate, labelText: place.formattedAddress)
        self.shouldFixOnUserLocation = false
    
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didFailAutocompleteWithError error: Error){
        Loger.error(message: "HOME VIEW CONTROLLER: Error on GMSAutocompleteResults \(error.localizedDescription)")
    }
    
    func didRequestAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
    }
    
    func didUpdateAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

extension HomeViewController : GMSMapViewDelegate{
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
     
        if gesture == true {
            self.shouldFixOnUserLocation = false
        }
        
    }
}
